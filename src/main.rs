mod rv;

use std::env;
use std::fs;
use std::path::Path;

use getopts::Options;
use goblin::elf::{program_header::PT_LOAD, Elf};

use rv::emulator::*;

fn run_bin(path: &Path, debug: bool, use_jit: bool) -> Result<(), ()> {
    let mut machine = Machine::<Rv32>::new();

    if use_jit {
        machine.use_jit();
    }

    let buffer = fs::read(path).unwrap();
    let elf = match Elf::parse(&buffer[..]) {
        Ok(elf) => elf,
        Err(e) => panic!("failed to parse elf: {}", e),
    };

    let base = elf.header.e_entry as u32;
    for header in elf.program_headers {
        if header.p_type != PT_LOAD {
            continue;
        }
        machine
            .memory()
            .map_memory_at(header.p_paddr as _, buffer[header.file_range()].into());
    }

    let mut tohost_addr = 0;
    for sym in elf.syms.iter() {
        if let Some(Ok("tohost")) = elf.strtab.get(sym.st_name) {
            tohost_addr = sym.st_value as u32;
        }
    }

    let mut hart = machine.make_hart(0);
    hart.set_pc(base);
    hart.set_debug_execution(debug);

    loop {
        hart.execute();
        let tohost = machine.memory().load_u32(tohost_addr).unwrap();
        if tohost & 1 != 0 {
            let err = tohost >> 1;
            if err != 0 {
                eprintln!("test failed: {}", err);
                return Err(());
            }
            break;
        }
    }
    println!("test passed");
    println!(
        "emulated {} instructions",
        hart.emulated_instruction_count()
    );

    Ok(())
}

fn main() -> Result<(), ()> {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optflag("g", "debug", "Debug Execution");
    opts.optflag("j", "jit", "Use dynamic recompilation.");
    let matches = opts.parse(&args[1..]).unwrap();
    let debug = matches.opt_present("g");
    let jit = matches.opt_present("j");

    for free in matches.free {
        println!("running test {}", free);
        run_bin(Path::new(&free), debug, jit)?;
    }

    Ok(())
}
