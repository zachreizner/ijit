# RISC-V Machine Notes

## mcause values

| Interrupt | Exception Code | Description |
|-|-|-|
| 1 | 0 | User software interrupt |
| 1 | 1 | Supervisor software interrupt |
| 1 | 2 | *Reserved* |
| 1 | 3 | Machine software interrupt |
|---|---|---|
| 1 | 4 | User timer interrupt |
| 1 | 5 | Supervisor timer interrupt |
| 1 | 6 | *Reserved* |
| 1 | 7 | Machine timer interrupt |
|---|---|---|
| 1 | 8 | User external interrupt |
| 1 | 9 | Supervisor external interrupt |
| 1 | 10 | *Reserved* |
| 1 | 11 | Machine external interrupt |
|---|---|---|
| 1 | ≥12 | *Reserved* |
|---|---|---|
| 0 | 0 | Instruction address misaligned |
| 0 | 1 | Instruction access fault |
| 0 | 2 | Illegal instruction |
| 0 | 3 | Breakpoint |
| 0 | 4 | Load address misaligned |
| 0 | 5 | Load access fault |
| 0 | 6 | Store/AMO address misaligned |
| 0 | 7 | Store/AMO access fault |
| 0 | 8 | Environment call from U-mode |
| 0 | 9 | Environment call from S-mode |
| 0 | 10 | *Reserved* |
| 0 | 11 | Environment call from M-mode |
| 0 | 12 | Instruction page fault |
| 0 | 13 | Load page fault |
| 0 | 14 | *Reserved* |
| 0 | 15 | Store/AMO page fault |


## CSRs

| Number | Privilege | Name | Description |
|-|-|-|-|
**User Trap Setup**
| 0x000 (0) | URW | ustatus | User status register.
| 0x004 (4) | URW | uie | User interrupt-enable register.
| 0x005 (5) | URW | utvec | User trap handler base address.
**User Trap Handling**
| 0x040 (64) | URW | uscratch | Scratch register for user trap handlers.
| 0x041 (65) | URW | uepc | User exception program counter.
| 0x042 (66) | URW | ucause | User trap cause.
| 0x043 (67) | URW | utval | User bad address or instruction.
| 0x044 (68) | URW | uip | User interrupt pending.
**User Floating-Point CSRs**
| 0x001 (1) | URW | fflags | Floating-Point Accrued Exceptions.
| 0x002 (2) | URW | frm | Floating-Point Dynamic Rounding Mode.
| 0x003 (3) | URW | fcsr | Floating-Point Control and Status Register (frm + fflags).
**User Counter/Timers**
| 0xC00 (3072) | URO | cycle | Cycle counter for RDCYCLE instruction.
| 0xC01 (3073) | URO | time | Timer for RDTIME instruction.
| 0xC02 (3074) | URO | instret | Instructions-retired counter for RDINSTRET instruction.
| 0xC03 (3075) | URO | hpmcounter3 | Performance-monitoring counter.
| 0xC04 (3076) | URO | hpmcounter4 | Performance-monitoring counter.
| 0xC1F (3103) | URO | hpmcounter31 | Performance-monitoring counter.
| 0xC80 (3200) | URO | cycleh | Upper 32 bits of cycle, RV32I only.
| 0xC81 (3201) | URO | timeh | Upper 32 bits of time, RV32I only.
| 0xC82 (3202) | URO | instreth | Upper 32 bits of instret, RV32I only.
| 0xC83 (3203) | URO | hpmcounter3h | Upper 32 bits of hpmcounter3, RV32I only.
| 0xC84 (3204) | URO | hpmcounter4h | Upper 32 bits of hpmcounter4, RV32I only.
| 0xC9F (3231) | URO | hpmcounter31h | Upper 32 bits of hpmcounter31, RV32I only.
**Supervisor Trap Setup**
| 0x100 (256) | SRW | sstatus | Supervisor status register.
| 0x102 (258) | SRW | sedeleg | Supervisor exception delegation register.
| 0x103 (259) | SRW | sideleg | Supervisor interrupt delegation register.
| 0x104 (260) | SRW | sie | Supervisor interrupt-enable register.
| 0x105 (261) | SRW | stvec | Supervisor trap handler base address.
| 0x106 (262) | SRW | scounteren | Supervisor counter enable.
**Supervisor Trap Handling**
| 0x140 (320) | SRW | sscratch | Scratch register for supervisor trap handlers.
| 0x141 (321) | SRW | sepc | Supervisor exception program counter.
| 0x142 (322) | SRW | scause | Supervisor trap cause.
| 0x143 (323) | SRW | stval | Supervisor bad address or instruction.
| 0x144 (324) | SRW | sip | Supervisor interrupt pending.
**Supervisor Protection and Translation**
| 0x180 (384) | SRW | satp | Supervisor address translation and protection.
**Machine Information Registers**
| 0xF11 (3857) | MRO | mvendorid | Vendor ID.
| 0xF12 (3858) | MRO | marchid | Architecture ID.
| 0xF13 (3859) | MRO | mimpid | Implementation ID.
| 0xF14 (3860) | MRO | mhartid | Hardware thread ID.
**Machine Trap Setup**
| 0x300 (768) | MRW | mstatus | Machine status register.
| 0x301 (769) | MRW | misa | ISA and extensions
| 0x302 (770) | MRW | medeleg | Machine exception delegation register.
| 0x303 (771) | MRW | mideleg | Machine interrupt delegation register.
| 0x304 (772) | MRW | mie | Machine interrupt-enable register.
| 0x305 (773) | MRW | mtvec | Machine trap-handler base address.
| 0x306 (774) | MRW | mcounteren | Machine counter enable.
**Machine Trap Handling**
| 0x340 (832) | MRW | mscratch | Scratch register for machine trap handlers.
| 0x341 (833) | MRW | mepc | Machine exception program counter.
| 0x342 (834) | MRW | mcause | Machine trap cause.
| 0x343 (835) | MRW | mtval | Machine bad address or instruction.
| 0x344 (836) | MRW | mip | Machine interrupt pending.
**Machine Protection and Translation**
| 0x3A0 (928) | MRW | pmpcfg0 | Physical memory protection configuration.
| 0x3A1 (929) | MRW | pmpcfg1 | Physical memory protection configuration, RV32 only.
| 0x3A2 (930) | MRW | pmpcfg2 | Physical memory protection configuration.
| 0x3A3 (931) | MRW | pmpcfg3 | Physical memory protection configuration, RV32 only.
| 0x3B0 (944) | MRW | pmpaddr0 | Physical memory protection address register.
| 0x3B1 (945) | MRW | pmpaddr1 | Physical memory protection address register.
| 0x3BF (959) | MRW | pmpaddr15 | Physical memory protection address register.
**Machine Counter/Timers**
| 0xB00 (2816) | MRW | mcycle | Machine cycle counter.
| 0xB02 (2818) | MRW | minstret | Machine instructions-retired counter.
| 0xB03 (2819) | MRW | mhpmcounter3 | Machine performance-monitoring counter.
| 0xB04 (2820) | MRW | mhpmcounter4 | Machine performance-monitoring counter.
| 0xB1F (2847) | MRW | mhpmcounter31 | Machine performance-monitoring counter.
| 0xB80 (2944) | MRW | mcycleh | Upper 32 bits of mcycle, RV32I only.
| 0xB82 (2946) | MRW | minstreth | Upper 32 bits of minstret, RV32I only.
| 0xB83 (2947) | MRW | mhpmcounter3h | Upper 32 bits of mhpmcounter3, RV32I only.
| 0xB84 (2948) | MRW | mhpmcounter4h | Upper 32 bits of mhpmcounter4, RV32I only.
| 0xB9F (2975) | MRW | mhpmcounter31h | Upper 32 bits of mhpmcounter31, RV32I only.
**Machine Counter Setup**
| 0x323 (803) | MRW | mhpmevent3 | Machine performance-monitoring event selector.
| 0x324 (804) | MRW | mhpmevent4 | Machine performance-monitoring event selector.
| 0x33F (831) | MRW | mhpmevent31 | Machine performance-monitoring event selector.
**Debug/Trace Registers (shared with Debug Mode)**
| 0x7A0 (1952) | MRW | tselect | Debug/Trace trigger register select.
| 0x7A1 (1953) | MRW | tdata1 | First Debug/Trace trigger data register.
| 0x7A2 (1954) | MRW | tdata2 | Second Debug/Trace trigger data register.
| 0x7A3 (1955) | MRW | tdata3 | Third Debug/Trace trigger data register.
**Debug Mode Registers**
| 0x7B0 (1968) | DRW | dcsr | Debug control and status register.
| 0x7B1 (1969) | DRW | dpc | Debug PC.
| 0x7B2 (1970) | DRW | dscratch | Debug scratch register.