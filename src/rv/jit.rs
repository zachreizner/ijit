use std::collections::hash_map::Entry;
use std::collections::{BTreeMap, HashMap};
use std::marker::PhantomData;
use std::mem::size_of;

use cranelift::prelude::*;
use cranelift_jit::{JITBuilder, JITModule};
use cranelift_module::{Linkage, Module};

use super::emulator::Memory;
use super::RvDecoded;
use crate::rv::Rv32Inst;
use crate::Word;

pub type BasicBlockFn<W> = fn(registers: &mut [W; 32]) -> W;

struct BlockCacheEntry {
    block: Block,
    compiled: bool,
}

impl From<Block> for BlockCacheEntry {
    fn from(block: Block) -> Self {
        BlockCacheEntry {
            block,
            compiled: false,
        }
    }
}

#[derive(Default)]
struct BlockCache<W: Word> {
    blocks: BTreeMap<W, BlockCacheEntry>,
}

impl<W: Word> BlockCache<W> {
    pub fn block_at_pc(&mut self, builder: &mut FunctionBuilder, pc: W) -> Block {
        self.blocks
            .entry(pc)
            .or_insert_with(|| builder.create_block().into())
            .block
    }

    pub fn compile_next(&mut self) -> Option<(W, Block)> {
        for (&pc, entry) in self.blocks.iter_mut() {
            if !entry.compiled {
                entry.compiled = true;
                return Some((pc, entry.block));
            }
        }

        None
    }
}

enum TranslateOutcome {
    Good,
    Branch,
    Emulate,
}

pub struct JIT<W: Word> {
    ctx: codegen::Context,
    module: JITModule,
    compile_cache: HashMap<W, BasicBlockFn<W>>,
    _phantom: PhantomData<W>,
}

impl<W: Word> Default for JIT<W> {
    fn default() -> Self {
        let mut flag_builder = settings::builder();
        flag_builder.set("is_pic", "true").unwrap();
        flag_builder.set("opt_level", "speed").unwrap();
        let isa_builder = cranelift_native::builder().unwrap_or_else(|msg| {
            panic!("host machine is not supported: {}", msg);
        });
        let isa = isa_builder.finish(settings::Flags::new(flag_builder));

        let mut builder = JITBuilder::with_isa(isa, cranelift_module::default_libcall_names());
        fn external_function() -> isize {
            eprintln!("external function was called");
            1
        }
        builder.symbol("external_function", external_function as *const _);
        let module = JITModule::new(builder);
        Self {
            ctx: module.make_context(),
            module,
            compile_cache: Default::default(),
            _phantom: PhantomData,
        }
    }
}

const fn word_type<W>() -> Type {
    let word_size = size_of::<W>();
    match word_size {
        4 => types::I32,
        8 => types::I64,
        _ => types::INVALID,
    }
}

impl<W: Word> JIT<W> {
    const WORD_TYPE: Type = word_type::<W>();

    /// Compile a string in the toy language into machine code.
    pub fn compile(&mut self, pc: W, memory: &Memory<W>) -> Result<BasicBlockFn<W>, String> {
        let cache_entry = self.compile_cache.entry(pc);
        if let Entry::Occupied(entry) = cache_entry {
            return Ok(*entry.get());
        }
        let name = format!("entry_point_{:x}", pc);
        let word_size = size_of::<W>();
        // Our toy language currently only supports I64 values, though Cranelift
        // supports other types.
        let pointer_type = self.module.target_config().pointer_type();

        self.module.clear_context(&mut self.ctx);
        self.ctx
            .func
            .signature
            .params
            .push(AbiParam::new(pointer_type));

        // Our toy language currently only supports one return value, though
        // Cranelift is designed to support more.
        self.ctx
            .func
            .signature
            .returns
            .push(AbiParam::new(Self::WORD_TYPE));

        // Create the builder to build a function.
        let mut builder_context = FunctionBuilderContext::new();
        let mut builder = FunctionBuilder::new(&mut self.ctx.func, &mut builder_context);

        // Create the entry block, to start emitting code in.
        let entry_block = builder.create_block();

        // Since this is the entry block, add block parameters corresponding to
        // the function's parameters.
        //
        // TODO: Streamline the API here.
        builder.append_block_params_for_function_params(entry_block);

        // Tell the builder to emit code in this block.
        builder.switch_to_block(entry_block);

        // And, tell the builder that this block will have no further
        // predecessors. Since it's the entry block, it won't have any
        // predecessors.
        builder.seal_block(entry_block);

        let register_pointer = builder.block_params(entry_block)[0];

        // Load the registers array into variables.
        for reg_idx in 1..32 {
            let offset = reg_idx * word_size as u32;
            let reg_var = Variable::with_u32(reg_idx);
            let reg_val = builder.ins().load(
                Self::WORD_TYPE,
                MemFlags::trusted(),
                register_pointer,
                offset as i32,
            );
            builder.declare_var(reg_var, Self::WORD_TYPE);
            builder.def_var(reg_var, reg_val);
        }

        let mut block_cache = BlockCache::default();
        let main_block = block_cache.block_at_pc(&mut builder, pc);
        builder.ins().jump(main_block, &[]);

        let exit_block = builder.create_block();
        let return_pc = builder.append_block_param(exit_block, Self::WORD_TYPE);
        builder.switch_to_block(exit_block);

        // Put the register variables into the register array
        for reg_idx in 1..32 {
            let offset = reg_idx * word_size as u32;
            let reg_val = Self::use_reg(&mut builder, reg_idx as u8);
            builder.ins().store(
                MemFlags::trusted(),
                reg_val,
                register_pointer,
                offset as i32,
            );
        }
        builder.ins().return_(&[return_pc]);

        let mut instructions_translated = 0;
        while let Some((mut pc, block)) = block_cache.compile_next() {
            builder.switch_to_block(block);
            loop {
                match memory.load_u32(pc) {
                    Some(v) => {
                        let inst = Rv32Inst(v);
                        let decoded = inst.decode();

                        let next_pc = pc.add_imm(decoded.size as i32);

                        match Self::translate_inst(&mut builder, &mut block_cache, pc, &decoded) {
                            TranslateOutcome::Good => {}
                            TranslateOutcome::Branch => {
                                // An instruction that branches, even conditionally, must be
                                // followed by an unconditional jump, so we start one here.
                                let fallthrough_block =
                                    block_cache.block_at_pc(&mut builder, next_pc);
                                builder.ins().jump(fallthrough_block, &[]);
                                break;
                            }
                            TranslateOutcome::Emulate => {
                                let pc_val =
                                    builder.ins().iconst(Self::WORD_TYPE, pc.as_u64() as i64);
                                builder.ins().jump(exit_block, &[pc_val]);
                                break;
                            }
                        }

                        pc = next_pc;
                        instructions_translated += 1;
                    }
                    None => {
                        break;
                    }
                }
            }
        }

        if instructions_translated == 0 {
            self.module.clear_context(&mut self.ctx);
            return Err("zero instructions were translated".to_owned());
        }

        /*
        let ext_func = self
            .module
            .declare_function("external_function", Linkage::Import, &sig)
            .expect("problem declaring function");
        let local_func = self
            .module
            .declare_func_in_func(ext_func, &mut builder.func);
        let call_inst = builder.ins().call(local_func, &[]);
        let ret0 = builder.inst_results(call_inst)[0];
        */

        builder.seal_all_blocks();

        // Tell the builder we're done with this function.
        builder.finalize();
        // eprintln!(
        //     "translated {} instruction\n{}",
        //     instructions_translated,
        //     builder.display(None)
        // );

        // Next, declare the function to jit. Functions must be declared
        // before they can be called, or defined.
        //
        // TODO: This may be an area where the API should be streamlined; should
        // we have a version of `declare_function` that automatically declares
        // the function?
        let id = self
            .module
            .declare_function(&name, Linkage::Export, &self.ctx.func.signature)
            .expect("failed to declare function");
        // .map_err(|e| e.to_string())?;

        // Define the function to jit. This finishes compilation, although
        // there may be outstanding relocations to perform. Currently, jit
        // cannot finish relocations until all functions to be called are
        // defined. For this toy demo for now, we'll just finalize the
        // function below.
        self.module
            .define_function(
                id,
                &mut self.ctx,
                &mut codegen::binemit::NullTrapSink {},
                &mut codegen::binemit::NullStackMapSink {},
            )
            .expect("failed to define function");
        // .map_err(|e| e.to_string())?;

        // Now that compilation is finished, we can clear out the context state.
        self.module.clear_context(&mut self.ctx);

        // Finalize the functions which we just defined, which resolves any
        // outstanding relocations (patching in addresses, now that they're
        // available).
        self.module.finalize_definitions();

        // We can now retrieve a pointer to the machine code.
        let code = self.module.get_finalized_function(id);

        let func = unsafe { std::mem::transmute(code) };
        cache_entry.or_insert(func);

        Ok(func)
    }

    fn use_reg(builder: &mut FunctionBuilder, reg_idx: u8) -> Value {
        match reg_idx {
            0 => builder.ins().iconst(Self::WORD_TYPE, 0),
            1..=32 => builder.use_var(Variable::with_u32(reg_idx as u32)),
            _ => {
                panic!("invalid register index {}", reg_idx)
            }
        }
    }

    fn assign_reg(builder: &mut FunctionBuilder, reg_idx: u8, val: Value) {
        match reg_idx {
            0 => (),
            1..=32 => builder.def_var(Variable::with_u32(reg_idx as u32), val),
            _ => {
                panic!("invalid register index {}", reg_idx)
            }
        }
    }

    fn translate_inst(
        builder: &mut FunctionBuilder,
        block_cache: &mut BlockCache<W>,
        pc: W,
        inst: &RvDecoded,
    ) -> TranslateOutcome {
        use super::RvOpName::*;
        match inst.name {
            // SW | SH | SB => {
            //     let address = rs1.add_imm(imm);
            //     let fault = match inst.name {
            //         SW => !self.memory.store_u32(address, rs2.as_u32()),
            //         SH => !self.memory.store_u16(address, rs2.as_u16()),
            //         SB => !self.memory.store_u8(address, rs2.as_u8()),
            //         _ => unreachable!(),
            //     };
            //     if fault {
            //         self.cause_trap(RvMCause::StoreAccessFault)
            //     }
            // }
            // LW => {
            //     let address = rs1.add_imm(imm);
            //     match self.memory.load_u32(address) {
            //         Some(v) => rd = X::Word::from_u32(v),
            //         None => self.cause_trap(RvMCause::LoadAccessFault),
            //     }
            // }
            // LH | LHU => {
            //     let address = rs1.add_imm(imm);
            //     match self.memory.load_u16(address) {
            //         Some(v) => {
            //             let v = if inst.name == LH {
            //                 ((v as i16) as i32) as u32
            //             } else {
            //                 v as u32
            //             };
            //             rd = X::Word::from_u32(v);
            //         }
            //         None => self.cause_trap(RvMCause::LoadAccessFault),
            //     }
            // }
            // LB | LBU => {
            //     let address = rs1.add_imm(imm);
            //     match self.memory.load_u8(address) {
            //         Some(v) => {
            //             let v = if inst.name == LB {
            //                 ((v as i8) as i32) as u32
            //             } else {
            //                 v as u32
            //             };
            //             rd = X::Word::from_u32(v);
            //         }
            //         None => self.cause_trap(RvMCause::LoadAccessFault),
            //     }
            // }
            // JAL => {
            //     let link_pc = pc.add_imm(inst.size as _);
            //     let mut branch_pc = pc.add_imm(inst.imm);
            //     return false;
            // }
            // JALR => {
            //     rd = self.next_pc;
            //     take_branch = true;
            //     branch_pc = rs1.add_imm(imm).clear_ls_bits(1);
            // }
            AUIPC => {
                let imm_val = builder
                    .ins()
                    .iconst(Self::WORD_TYPE, pc.add_imm(inst.imm).as_u64() as i64);
                Self::assign_reg(builder, inst.rd, imm_val)
            }
            LUI => {
                let imm_val = builder
                    .ins()
                    .iconst(Self::WORD_TYPE, (inst.imm as u32) as i64);
                Self::assign_reg(builder, inst.rd, imm_val)
            }
            SLL | SRL | SRA | ADD | SUB | XOR | OR | AND | SLT | SLTU => {
                let rs1_val = Self::use_reg(builder, inst.rs1);
                let rs2_val = Self::use_reg(builder, inst.rs2);
                let val = match inst.name {
                    SLL => builder.ins().ishl(rs1_val, rs2_val),
                    SRL => builder.ins().ushr(rs1_val, rs2_val),
                    SRA => builder.ins().sshr(rs1_val, rs2_val),
                    ADD => builder.ins().iadd(rs1_val, rs2_val),
                    SUB => builder.ins().isub(rs1_val, rs2_val),
                    XOR => builder.ins().bxor(rs1_val, rs2_val),
                    OR => builder.ins().bor(rs1_val, rs2_val),
                    AND => builder.ins().band(rs1_val, rs2_val),
                    SLT | SLTU => {
                        let condition = if inst.name == SLTU {
                            IntCC::UnsignedLessThan
                        } else {
                            IntCC::SignedLessThan
                        };
                        let cmp_val = builder.ins().icmp(condition, rs1_val, rs2_val);
                        builder.ins().bint(Self::WORD_TYPE, cmp_val)
                    }
                    _ => unreachable!(),
                };

                Self::assign_reg(builder, inst.rd, val)
            }
            SLLI | SRLI | SRAI | ADDI | XORI | ORI | ANDI | SLTI | SLTIU => {
                let rs1_val = Self::use_reg(builder, inst.rs1);
                let imm = (inst.imm as u32) as i64;
                let val = match inst.name {
                    SLLI => builder.ins().ishl_imm(rs1_val, imm),
                    SRLI => builder.ins().ushr_imm(rs1_val, imm),
                    SRAI => builder.ins().sshr_imm(rs1_val, imm),
                    ADDI => builder.ins().iadd_imm(rs1_val, imm),
                    XORI => builder.ins().bxor_imm(rs1_val, imm),
                    ORI => builder.ins().bor_imm(rs1_val, imm),
                    ANDI => builder.ins().band_imm(rs1_val, imm),
                    SLTI | SLTIU => {
                        let condition = if inst.name == SLTIU {
                            IntCC::UnsignedLessThan
                        } else {
                            IntCC::SignedLessThan
                        };
                        let cmp_val = builder.ins().icmp_imm(condition, rs1_val, imm);
                        builder.ins().bint(Self::WORD_TYPE, cmp_val)
                    }
                    _ => unreachable!(),
                };
                Self::assign_reg(builder, inst.rd, val)
            }
            BNE | BEQ | BLT | BLTU | BGE | BGEU => {
                let condition = match inst.name {
                    BNE => IntCC::NotEqual,
                    BEQ => IntCC::Equal,
                    BLT => IntCC::SignedLessThan,
                    BLTU => IntCC::UnsignedLessThan,
                    BGE => IntCC::SignedGreaterThanOrEqual,
                    BGEU => IntCC::UnsignedGreaterThanOrEqual,
                    _ => unreachable!(),
                };
                let rs1_val = Self::use_reg(builder, inst.rs1);
                let rs2_val = Self::use_reg(builder, inst.rs2);
                let branch_pc = pc.add_imm(inst.imm);
                let block = block_cache.block_at_pc(builder, branch_pc);
                builder
                    .ins()
                    .br_icmp(condition, rs1_val, rs2_val, block, &[]);
                return TranslateOutcome::Branch;
            }
            _ => return TranslateOutcome::Emulate,
        }
        TranslateOutcome::Good
    }
}
