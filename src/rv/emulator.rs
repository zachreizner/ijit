use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::convert::TryInto;
use std::fmt::{Binary, Debug, LowerHex, UpperHex};
use std::hash::Hash;
use std::marker::PhantomData;
use std::mem::size_of;
use std::ops::{Add, BitAnd, BitOr, BitXor, Div, Mul, Not, Shl, Shr, Sub};
use std::sync::{Arc, Mutex};

use super::jit::JIT;
use super::{Rv32Inst, RvCsr, RvDecoded, RvMCause};

pub trait Word:
    Add<Output = Self>
    + Binary
    + BitAnd<Output = Self>
    + BitOr<Output = Self>
    + BitXor<Output = Self>
    + Clone
    + Copy
    + Debug
    + Default
    + Div
    + Eq
    + Hash
    + LowerHex
    + Mul
    + Not<Output = Self>
    + Ord
    + PartialEq
    + PartialOrd
    + Shl<i32, Output = Self>
    + Shl<Self, Output = Self>
    + Shr<i32, Output = Self>
    + Shr<Self, Output = Self>
    + Sub<Output = Self>
    + UpperHex
{
    fn to_slice(self, out: &mut [u8]);
    fn as_u8(self) -> u8;
    fn as_u16(self) -> u16;
    fn as_u32(self) -> u32;
    fn as_u64(self) -> u64;
    fn as_usize(self) -> usize;
    fn from_imm(v: i32) -> Self;
    fn from_u32(v: u32) -> Self;
    fn from_usize(v: usize) -> Self;
    fn from_slice(v: &[u8]) -> Self;
    fn signed_shr(self, rhs: Self) -> Self;
    fn add_imm(self, imm: i32) -> Self;
    fn add_word(self, rhs: Self) -> Self;
    fn sub_imm(self, imm: i32) -> Self;
    fn sub_word(self, rhs: Self) -> Self;
    fn is_aligned(self, alignment_bits: i32) -> bool;
    fn clear_ls_bits(self, bits: i32) -> Self;
    fn signed_cmp(self, rhs: Self) -> Ordering;
    fn wrapping_shl(self, rhs: Self) -> Self;
}

impl Word for u32 {
    fn to_slice(self, out: &mut [u8]) {
        let arr: &mut [u8; 4] = out.try_into().unwrap();
        *arr = self.to_le_bytes()
    }

    fn as_u8(self) -> u8 {
        self as _
    }

    fn as_u16(self) -> u16 {
        self as _
    }

    fn as_u32(self) -> u32 {
        self as _
    }

    fn as_u64(self) -> u64 {
        self as _
    }

    fn as_usize(self) -> usize {
        self as _
    }

    fn from_imm(v: i32) -> Self {
        v as _
    }

    fn from_u32(v: u32) -> Self {
        v as _
    }

    fn from_usize(v: usize) -> Self {
        v as _
    }

    fn from_slice(v: &[u8]) -> Self {
        Self::from_le_bytes(v.try_into().unwrap())
    }

    fn signed_shr(self, rhs: Self) -> Self {
        ((self as i32) >> ((rhs & 0x1f) as i32)) as _
    }

    fn add_imm(self, imm: i32) -> Self {
        (self as i32).wrapping_add(imm) as _
    }

    fn add_word(self, rhs: Self) -> Self {
        self.wrapping_add(rhs)
    }

    fn sub_imm(self, imm: i32) -> Self {
        (self as i32).wrapping_sub(imm) as _
    }

    fn sub_word(self, rhs: Self) -> Self {
        self.wrapping_sub(rhs)
    }

    fn is_aligned(self, alignment_bits: i32) -> bool {
        self & ((1 << alignment_bits) - 1) == 0
    }

    fn clear_ls_bits(self, bits: i32) -> Self {
        self & !((1 << bits) - 1)
    }

    fn signed_cmp(self, rhs: Self) -> Ordering {
        (self as i32).cmp(&(rhs as i32))
    }

    fn wrapping_shl(self, rhs: Self) -> Self {
        self.wrapping_shl(rhs)
    }
}
// impl Word for u64 {}
// impl Word for u128 {}

pub trait Arch: Default + Clone + 'static {
    type Word: Word;
}

#[derive(Clone, Default)]
pub struct Rv32;

impl Arch for Rv32 {
    type Word = u32;
}

#[derive(Default, Clone)]
pub struct Overlay<X: Arch> {
    sparse: BTreeMap<X::Word, X::Word>,
}

impl<X: Arch> Overlay<X> {
    #[allow(dead_code)]
    fn new() -> Self {
        Self {
            sparse: Default::default(),
        }
    }
}

pub trait ControlStatusReg<X: Arch> {
    fn raw_value(&self, _hart: &mut Hart<X>) -> X::Word {
        Default::default()
    }

    fn read_write(&mut self, _hart: &mut Hart<X>, _in: X::Word, _out: Option<&mut X::Word>) {
        Default::default()
    }

    fn read_set(&mut self, _hart: &mut Hart<X>, _in: Option<X::Word>) -> X::Word {
        Default::default()
    }

    fn read_clear(&mut self, _hart: &mut Hart<X>, _in: Option<X::Word>) -> X::Word {
        Default::default()
    }
}

pub trait SimpleCSR<X: Arch>: Default {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.cause_trap(RvMCause::IllegalInstruction);
        Default::default()
    }
    fn write(&mut self, hart: &mut Hart<X>, _in: X::Word) {
        hart.cause_trap(RvMCause::IllegalInstruction);
    }
}

#[derive(Default)]
struct SimpleCSRWrapper<T: SimpleCSR<X>, X: Arch>(T, PhantomData<X>);

impl<T: SimpleCSR<X>, X: Arch> ControlStatusReg<X> for SimpleCSRWrapper<T, X> {
    fn raw_value(&self, hart: &mut Hart<X>) -> X::Word {
        self.0.read(hart)
    }

    fn read_write(&mut self, hart: &mut Hart<X>, in_: X::Word, out: Option<&mut X::Word>) {
        let read_value = self.0.read(hart);
        // TODO: if trapped, avoid writing.
        self.0.write(hart, in_);
        out.map(|o| *o = read_value);
    }

    fn read_set(&mut self, hart: &mut Hart<X>, in_: Option<X::Word>) -> X::Word {
        let read_value = self.0.read(hart);
        // TODO: if trapped, avoid writing.
        if let Some(in_) = in_ {
            self.0.write(hart, read_value | in_);
        }
        read_value
    }

    fn read_clear(&mut self, hart: &mut Hart<X>, in_: Option<X::Word>) -> X::Word {
        let read_value = self.0.read(hart);
        // TODO: if trapped, avoid writing.
        if let Some(in_) = in_ {
            self.0.write(hart, read_value & (!in_));
        }
        read_value
    }
}

#[derive(Default)]
struct CsrMstatus<X: Arch>(PhantomData<X>);
impl<X: Arch> SimpleCSR<X> for CsrMstatus<X> {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.mstatus
    }
    fn write(&mut self, hart: &mut Hart<X>, in_: X::Word) {
        hart.mstatus = in_;
    }
}

#[derive(Default)]
struct CsrMtvec<X: Arch>(PhantomData<X>);
impl<X: Arch> SimpleCSR<X> for CsrMtvec<X> {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.mtvec
    }
    fn write(&mut self, hart: &mut Hart<X>, in_: X::Word) {
        hart.mtvec = in_;
    }
}

#[derive(Default)]
struct CsrMepc<X: Arch>(PhantomData<X>);
impl<X: Arch> SimpleCSR<X> for CsrMepc<X> {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.mepc
    }
    fn write(&mut self, hart: &mut Hart<X>, in_: X::Word) {
        hart.mepc = in_;
    }
}

#[derive(Default)]
struct CsrMHarthid<X: Arch>(PhantomData<X>);
impl<X: Arch> SimpleCSR<X> for CsrMHarthid<X> {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.hart_id
    }
}

#[derive(Default)]
struct CsrMCause<X: Arch>(X::Word);
impl<X: Arch> SimpleCSR<X> for CsrMCause<X> {
    fn read(&self, hart: &mut Hart<X>) -> X::Word {
        hart.mcause
    }
    fn write(&mut self, hart: &mut Hart<X>, in_: X::Word) {
        hart.mcause = in_;
    }
}

#[derive(Default)]
struct CsrGenericRW<X: Arch>(X::Word);
impl<X: Arch> SimpleCSR<X> for CsrGenericRW<X> {
    fn read(&self, _hart: &mut Hart<X>) -> X::Word {
        self.0
    }
    fn write(&mut self, _hart: &mut Hart<X>, in_: X::Word) {
        self.0 = in_;
    }
}
#[derive(Default, Clone)]
pub struct ControlStatusRegs<X: Arch> {
    registers: Arc<Mutex<BTreeMap<u16, Box<dyn ControlStatusReg<X>>>>>,
}

impl<X: Arch> ControlStatusRegs<X> {
    fn new() -> Self {
        let mut regs: BTreeMap<u16, Box<dyn ControlStatusReg<X>>> = Default::default();
        // regs.insert(RvCsr::MHARTID as _, Box::new(SimpleCSRWrapper::<CsrMHarthid<X>, _>::default()));
        Self::insert_simple::<CsrMstatus<X>>(&mut regs, RvCsr::MSTATUS as _);
        Self::insert_simple::<CsrMtvec<X>>(&mut regs, RvCsr::MTVEC as _);
        Self::insert_simple::<CsrMepc<X>>(&mut regs, RvCsr::MEPC as _);
        Self::insert_simple::<CsrMHarthid<X>>(&mut regs, RvCsr::MHARTID as _);
        Self::insert_simple::<CsrMCause<X>>(&mut regs, RvCsr::MCAUSE as _);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, RvCsr::SATP as _);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, RvCsr::PMPADDR0 as _);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, RvCsr::PMPCFG0 as _);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, 0x302);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, 0x303);
        Self::insert_simple::<CsrGenericRW<X>>(&mut regs, 0x304);
        Self {
            registers: Arc::new(Mutex::new(regs)),
        }
    }

    fn insert_simple<T: 'static + SimpleCSR<X>>(
        regs: &mut BTreeMap<u16, Box<dyn ControlStatusReg<X>>>,
        id: u16,
    ) {
        regs.insert(id, Box::new(SimpleCSRWrapper::<T, _>::default()));
    }

    #[allow(dead_code)]
    fn get_raw_value(&self, hart: &mut Hart<X>, id: u16) -> Option<X::Word> {
        Some(self.registers.lock().unwrap().get(&id)?.raw_value(hart))
    }
}

#[derive(Default)]
pub struct Regs<X: Arch>([X::Word; 32]);

impl<X: Arch> Regs<X> {
    pub fn get(&self, idx: u8) -> X::Word {
        if idx == 0 {
            return Default::default();
        }
        self.0[idx as usize]
    }

    pub fn set(&mut self, idx: u8, value: X::Word) {
        if idx == 0 {
            return;
        }
        self.0[idx as usize] = value;
    }

    pub fn as_mut(&mut self) -> &mut [X::Word; 32] {
        &mut self.0
    }
}

// pub enum MemoryAccess<X: Arch> {
//     Load {
//         address: X::Word,
//         size: u8,
//     },
//     Store {
//         address: X::Word,
//         value: X::Word,
//         size: u8,
//     },
// }

#[derive(Default, Clone)]
pub struct Memory<W: Word> {
    memory_mappings: Arc<Mutex<BTreeMap<W, Box<[u8]>>>>,
}

impl<W: Word> Memory<W> {
    pub fn map_memory_at(&self, address: W, mem: Box<[u8]>) {
        // TODO: check for overlap
        self.memory_mappings.lock().unwrap().insert(address, mem);
    }

    #[allow(dead_code)]
    pub fn insert_memory(&self, address: W, len: W) {
        // TODO: check for overlap
        self.memory_mappings
            .lock()
            .unwrap()
            .insert(address, vec![0; len.as_usize()].into());
    }

    pub fn find_mapping_at<F, T>(&self, address: W, f: F) -> Option<T>
    where
        F: FnOnce(W, &mut [u8]) -> T,
    {
        for (&base, mem) in self.memory_mappings.lock().unwrap().iter_mut() {
            if address < base {
                continue;
            }
            let offset = address - base;
            if offset >= W::from_usize(mem.len()) {
                continue;
            }
            return Some(f(offset, mem));
        }
        None
    }

    pub fn load_u8(&self, address: W) -> Option<u8> {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            mem[offset]
        })
    }

    pub fn load_u16(&self, address: W) -> Option<u16> {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mem[offset..offset + size_of::<u16>()];
            u16::from_le_bytes(bytes.try_into().unwrap())
        })
    }

    pub fn load_u32(&self, address: W) -> Option<u32> {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mem[offset..offset + size_of::<u32>()];
            u32::from_le_bytes(bytes.try_into().unwrap())
        })
    }

    #[allow(dead_code)]
    pub fn load_u64(&self, address: W) -> Option<u64> {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mem[offset..offset + size_of::<u64>()];
            u64::from_le_bytes(bytes.try_into().unwrap())
        })
    }

    pub fn store_u8(&self, address: W, value: u8) -> bool {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            mem[offset] = value;
            ()
        })
        .is_some()
    }

    pub fn store_u16(&self, address: W, value: u16) -> bool {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mut mem[offset..offset + size_of::<u16>()];
            bytes.copy_from_slice(&value.to_le_bytes());
            ()
        })
        .is_some()
    }

    pub fn store_u32(&self, address: W, value: u32) -> bool {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mut mem[offset..offset + size_of::<u32>()];
            bytes.copy_from_slice(&value.to_le_bytes());
            ()
        })
        .is_some()
    }

    #[allow(dead_code)]
    pub fn store_u64(&self, address: W, value: u64) -> bool {
        self.find_mapping_at(address, |offset, mem| {
            let offset = offset.as_usize();
            let bytes = &mut mem[offset..offset + size_of::<u64>()];
            bytes.copy_from_slice(&value.to_le_bytes());
            ()
        })
        .is_some()
    }
}

#[derive(Default)]
pub struct Hart<X: Arch> {
    csrs: ControlStatusRegs<X>,
    registers: Regs<X>,
    memory: Memory<X::Word>,
    pc: X::Word,
    next_pc: X::Word,
    hart_id: X::Word,
    mstatus: X::Word,
    mepc: X::Word,
    mtvec: X::Word,
    mcause: X::Word,

    debug_execution: bool,
    emulated_instruction_count: u64,
    jit: Option<JIT<X::Word>>,
}

impl<X: Arch> Hart<X> {
    pub fn set_pc(&mut self, pc: X::Word) {
        self.pc = pc
    }

    pub fn set_debug_execution(&mut self, debug: bool) {
        self.debug_execution = debug;
    }

    pub fn cause_trap(&mut self, mcause: RvMCause) {
        self.trigger_trap(X::Word::from_usize(mcause as _))
    }

    pub fn trigger_trap(&mut self, mcause: X::Word) {
        if self.pc == self.mtvec.clear_ls_bits(2) {
            panic!("trap at trap handler");
        }
        if self.debug_execution {
            eprintln!("trap mcause={:?} mtvec={:x}", mcause, self.mtvec);
        }
        self.mcause = mcause;
        self.next_pc = self.mtvec.clear_ls_bits(2);
    }

    pub fn emulated_instruction_count(&self) -> u64 {
        self.emulated_instruction_count
    }

    pub fn execute_decoded(&mut self, inst: RvDecoded) {
        use super::RvOpName::*;
        let rs1 = self.registers.get(inst.rs1);
        let rs2 = self.registers.get(inst.rs2);
        let mut rd = self.registers.get(inst.rd);
        let imm = inst.imm;
        let imm_word = X::Word::from_imm(imm);
        let mut take_branch = false;
        let mut branch_pc = self.pc.add_imm(inst.imm);
        match inst.name {
            SW | SH | SB => {
                let address = rs1.add_imm(imm);
                let fault = match inst.name {
                    SW => !self.memory.store_u32(address, rs2.as_u32()),
                    SH => !self.memory.store_u16(address, rs2.as_u16()),
                    SB => !self.memory.store_u8(address, rs2.as_u8()),
                    _ => unreachable!(),
                };
                if fault {
                    self.cause_trap(RvMCause::StoreAccessFault)
                }
            }
            LW => {
                let address = rs1.add_imm(imm);
                match self.memory.load_u32(address) {
                    Some(v) => rd = X::Word::from_u32(v),
                    None => self.cause_trap(RvMCause::LoadAccessFault),
                }
            }
            LH | LHU => {
                let address = rs1.add_imm(imm);
                match self.memory.load_u16(address) {
                    Some(v) => {
                        let v = if inst.name == LH {
                            ((v as i16) as i32) as u32
                        } else {
                            v as u32
                        };
                        rd = X::Word::from_u32(v);
                    }
                    None => self.cause_trap(RvMCause::LoadAccessFault),
                }
            }
            LB | LBU => {
                let address = rs1.add_imm(imm);
                match self.memory.load_u8(address) {
                    Some(v) => {
                        let v = if inst.name == LB {
                            ((v as i8) as i32) as u32
                        } else {
                            v as u32
                        };
                        rd = X::Word::from_u32(v);
                    }
                    None => self.cause_trap(RvMCause::LoadAccessFault),
                }
            }
            JAL => {
                rd = self.next_pc;
                take_branch = true;
            }
            JALR => {
                rd = self.next_pc;
                take_branch = true;
                branch_pc = rs1.add_imm(imm).clear_ls_bits(1);
            }
            AUIPC => rd = self.pc.add_imm(inst.imm),
            LUI => rd = imm_word,
            SLL => rd = rs1.wrapping_shl(rs2),
            SLLI => rd = <X::Word as Shl<i32>>::shl(rs1, imm),
            SRL => rd = rs1 >> (rs2 & X::Word::from_u32(0x1f)),
            SRLI => rd = rs1 >> (imm_word & X::Word::from_u32(0x1f)),
            SRA => rd = rs1.signed_shr(rs2),
            SRAI => rd = rs1.signed_shr(imm_word),
            ADD => rd = rs1.add_word(rs2),
            ADDI => rd = rs1.add_imm(imm),
            SUB => rd = rs1.sub_word(rs2),
            XOR => rd = rs1 ^ rs2,
            XORI => rd = rs1 ^ imm_word,
            OR => rd = rs1 | rs2,
            ORI => rd = rs1 | imm_word,
            AND => rd = rs1 & rs2,
            ANDI => rd = rs1 & imm_word,
            SLT => rd = X::Word::from_u32((rs1.signed_cmp(rs2) == Ordering::Less) as u32),
            SLTI => rd = X::Word::from_u32((rs1.signed_cmp(imm_word) == Ordering::Less) as u32),
            SLTU => rd = X::Word::from_u32((rs1.cmp(&rs2) == Ordering::Less) as u32),
            SLTIU => rd = X::Word::from_u32((rs1.cmp(&imm_word) == Ordering::Less) as u32),
            BEQ => take_branch = rs1 == rs2,
            BNE => take_branch = rs1 != rs2,
            BLT => take_branch = rs1.signed_cmp(rs2) == Ordering::Less,
            BLTU => take_branch = rs1.cmp(&rs2) == Ordering::Less,
            BGE => take_branch = rs1.signed_cmp(rs2) != Ordering::Less,
            BGEU => take_branch = rs1.cmp(&rs2) != Ordering::Less,
            // No fencing needed. Everything is sequentially consistent.
            FENCE => {}
            FENCE_I => {}
            ECALL => {
                self.mepc = self.pc;
                self.cause_trap(RvMCause::EcallFromMMode)
            }
            MRET => self.next_pc = self.mepc,
            CSRRS | CSRRSI | CSRRW | CSRRWI => {
                let csr_num = (inst.imm as u16) & 0xfff; // Undo the sign extension.
                let csrs = self.csrs.registers.clone();
                let mut csrs_locked = csrs.lock().unwrap();
                if let Some(csr) = csrs_locked.get_mut(&csr_num) {
                    match inst.name {
                        CSRRS | CSRRSI => {
                            let out = csr.read_set(
                                self,
                                if inst.rs1 != 0 {
                                    Some(if inst.name == CSRRSI {
                                        X::Word::from_usize(inst.rs1 as usize)
                                    } else {
                                        rs1
                                    })
                                } else {
                                    None
                                },
                            );
                            // TODO: maybe don't do this on trap?
                            rd = out;
                        }
                        CSRRW | CSRRWI => {
                            let mut out = Default::default();
                            let out_opt = if inst.rd != 0 { Some(&mut out) } else { None };
                            csr.read_write(
                                self,
                                if inst.name == CSRRWI {
                                    X::Word::from_usize(inst.rs1 as usize)
                                } else {
                                    rs1
                                },
                                out_opt,
                            );
                            // TODO: maybe don't do this on trap?
                            rd = out;
                        }
                        _ => unreachable!(),
                    }
                } else {
                    self.cause_trap(RvMCause::IllegalInstruction);
                }
            }
            _ => todo!("implement instruction {:?}", inst),
        }
        if take_branch {
            self.next_pc = branch_pc;
        }
        self.registers.set(inst.rd, rd);
        self.emulated_instruction_count += 1;
    }

    fn execute_inst(&mut self) {
        if let Some(jit) = self.jit.as_mut() {
            match jit.compile(self.pc, &self.memory) {
                Ok(f) => {
                    self.next_pc = f(self.registers.as_mut());
                    return;
                }
                Err(_e) => {
                    // eprintln!("failed to compile at 0x{:x}: {}", self.pc, e);
                }
            }
        }
        match self.memory.load_u32(self.pc) {
            Some(v) => {
                let inst = Rv32Inst(v);
                let decoded = inst.decode();
                if self.debug_execution {
                    eprintln!("{:x} {:?}", self.pc, decoded);
                }
                self.execute_decoded(decoded);
            }
            None => {
                if self.debug_execution {
                    eprintln!("inst access fault 0x{:x}", self.pc);
                }
                self.cause_trap(RvMCause::InstructionAccessFault)
            }
        };
    }

    pub fn execute(&mut self) {
        self.next_pc = self.pc.add_imm(4);

        if self.pc.is_aligned(2) {
            self.execute_inst();
        } else {
            self.cause_trap(RvMCause::InstructionAddressMisaligned)
        }

        self.pc = self.next_pc;
    }
}

pub struct Machine<X: Arch> {
    csrs: ControlStatusRegs<X>,
    memory: Memory<X::Word>,
    use_jit: bool,
}

impl<X: Arch> Machine<X> {
    pub fn new() -> Machine<X> {
        Self {
            csrs: ControlStatusRegs::new(),
            memory: Memory::default(),
            use_jit: false,
        }
    }

    pub fn use_jit(&mut self) {
        self.use_jit = true;
    }

    pub fn memory(&self) -> &Memory<X::Word> {
        &self.memory
    }

    pub fn make_hart(&self, hart_id: X::Word) -> Hart<X> {
        Hart {
            csrs: self.csrs.clone(),
            memory: self.memory.clone(),
            hart_id,
            jit: if self.use_jit {
                Some(Default::default())
            } else {
                None
            },
            ..Default::default()
        }
    }
}
